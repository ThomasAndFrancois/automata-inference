/*
  basicEval.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/05/02 : Version 0
*/

#ifndef _basicEval_h
#define _basicEval_h

#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>

/*
    Evaluation function of DFA solution:
      Execute the DFA on the sample, 
      The fitness is the ratio of words from the sample which are corrected computed

*/
class BasicEval : public Eval<double> {
public:
    using Eval::sample;

    BasicEval(Sample & _sample) : Eval(_sample) {
    }

    virtual void operator()(Solution<double> & _solution) {
      unsigned correct = 0;

      for(auto word : sample.set) {
        if (_solution.accept(word) == word.accept) {
          correct++;
        }
      }

      _solution.fitness(((double) correct) / (double) sample.set.size());
    }

};


#endif